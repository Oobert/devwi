let Hapi = require('hapi');
let boom = require('boom');
let config = require('./config');

let server = new Hapi.Server();

server.connection({
    host: '127.0.0.1',
    port: config.http.port
});


function routeAndStart(err) {
    if (err) return console.log(err);
    server.route([
        {
            method: 'GET',
            path: '/{param*}',
            handler: {
                directory: {
                    index: true,
                    path: 'dist'
                }
            }
        }
    ]);

    server.ext('onPostHandler', (request, reply) => {
        const response = request.response;
        if (response.isBoom && response.output.statusCode === 404) {
            return reply.file('dist/index.html');
        }
        return reply.continue();
    });

    server.start(function () {
        console.log('Server running at:', server.info.uri);
    });
}

server.register([
    { register: require('inert') },
    { register: require('./api/rss')},
    { register: require('./api/event'), routes: { prefix: '/api' } },
    { register: require('./api/posts'), routes: { prefix: '/api' } },
    { register: require('./api/groups'), routes: { prefix: '/api' } },
    ], routeAndStart);
