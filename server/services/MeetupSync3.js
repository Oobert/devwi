const util = require('util');
let Raven = require('raven');
Raven.config('https://1bf2cdc8fdbf44bf8048f4818e527c95:5fafc030871c4ef9bfb78f6da17ee6c4@sentry.io/156120').install();

let dbUtils = require('../utilities/dbUtils');

let requestjs = require('request');//.defaults({'proxy':'http://127.0.0.1:8888'});
let querystring = require('querystring');


let meetupEventQuery = `select *
                        from events e
                        where e.meetupGroupId = ?
                          and e.start >= ?
                          and e.end <= ?;`;

let meetupEventUpdateQuery = `UPDATE events
                                SET
                                title = ?,
                                description = ?,
                                start = ?,
                                end = ?
                                WHERE meetupEventId = ?;`;

let meetupEventDeleteQuery = `DELETE FROM events
                                WHERE meetupGroupId = ?
                                  and meetupEventId not in (?)
                                  and start >= ?
                                  and end <= ?;`;



let sync = (start, end) => {

    let getMeetupRequestUrl = (id) => {

        let getParms = querystring.stringify(
            {
                sign: true,
                'photo-host': 'public',
                group_id: id,
                time: [start.getTime(), end.getTime()].join(','),
                status: 'upcoming,past',
                page: 20,
                key: '6441475e11d501d714b2b5023287148',
                format: 'json'
            }
        );
        return "https://api.meetup.com/2/events?" + getParms;
    };

    let getUserGroupMeetings = async groupInfo => {




        return new Promise((resolve, reject) => {
            let groupUrl = getMeetupRequestUrl(groupInfo.meetupGroupId);

            requestjs(groupUrl, (err, res, body) => {
                if (err) return reject(err);
                if (res.statusCode !== 200) return reject(new Error('Status Code:' + res.statusCode + ' ' + body));

                if (body === undefined ||
                    body === null ||
                    body.length === 0)
                {
                    return resolve(null);
                }

                let asJson = (JSON.parse(body));
                resolve(asJson);

            });
        });
    };

    let sleep = async (ms) => {
        return new Promise((resolve, reject) => {
            setTimeout(() => resolve(), ms);
        });
    };

    let insertGroupMeetings = async(groupInfo, connection) => {

        try {
            let groupMeetings = await getUserGroupMeetings(groupInfo);
            let dbGroupMeetings = await dbUtils.query(connection, meetupEventQuery, [groupInfo.meetupGroupId, start, end]);

            if (groupMeetings &&
                groupMeetings.results &&
                groupMeetings.results.length > 0) {

                let eventIds = [];

                for (let index = 0; index < groupMeetings.results.length; index++) {
                    let event = groupMeetings.results[index];
                    eventIds.push(event.id);

                    let foundEvent = dbGroupMeetings.value.find((item) => item.meetupEventId == event.id);

                    if (!foundEvent) {
                        await dbUtils.query(connection,
                            'INSERT INTO events SET ?',
                            {
                                type: 'meetup',
                                meetupGroupId: event.group.id,
                                name: event.group.name,
                                title: event.name,
                                description: event.description || '',
                                start: new Date(event.time),
                                end: new Date(event.time),
                                meetupEventId: event.id,
                                url: event.event_url
                            });
                    }
                    else if (foundEvent.title !== event.name ||
                            foundEvent.description !== event.description ||
                            foundEvent.start.getTime() !== new Date(event.time).getTime() ||
                            foundEvent.end.getTime() !== new Date(event.time).getTime() ) {

                        await dbUtils.query(connection, meetupEventUpdateQuery, [event.name, event.description || '', new Date(event.time), new Date(event.time), event.id]);
                    }
                }

                await dbUtils.query(connection, meetupEventDeleteQuery, [groupInfo.meetupGroupId, eventIds, start, end]);

            }
        }
        catch (err) {
            console.log(`doh!`, JSON.stringify(groupInfo), err);
            Raven.captureException(err, { extra: groupInfo});
        }
    };



    let doWork = async ()=> {
        let connection;
        try {

            connection = await dbUtils.getConnection();
            let groups = await dbUtils.query(connection, 'select * from meetupgroups');

            for (let index = 0; index < groups.value.length; index++){
                await insertGroupMeetings(groups.value[index], connection);
                await sleep(3000);
            }
        }
        catch (err) {
            console.log('shit', err);
            Raven.captureException(err);
        }
        finally {
            connection.release();
        }
    };

    doWork();
};




var syncTimeout = 1000 * 60 * 60 * 4; //run every hour

 setInterval(() => {
     let start = new Date();
     start = new Date(start.setDate(start.getDate() - 7));

     let end = new Date();
     end = new Date(end.setDate(end.getDate() + 365));

     sync(start, end);
}, syncTimeout);


let start = new Date();
start = new Date(start.setDate(start.getDate() - 7));

let end = new Date();
end = new Date(end.setDate(end.getDate() + 365));

sync(start, end);