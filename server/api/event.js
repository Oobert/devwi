'use strict';
let boom = require('boom');
let dbUtils = require('../utilities/dbUtils');


function getEvents (request, reply) {

    var start = new Date(request.query.start);
    var end = new Date(request.query.end);

    dbUtils.getConnection()
        .then((connection) => dbUtils.query(connection, 'SELECT * FROM events e where e.start >= ? and e.start <= ?;', [start, end]))
        .then((queryResults) => {
            let events = [];
            queryResults.value.forEach((item) => {
                events.push({
                    id: item.id,
                    title: item.name + ' - ' + item.title,
                    allDay: false,
                    start: item.start.getTime(),
                    end: item.end.getTime(),
                    url: item.url,
                    description: item.description
                });
            });
            queryResults.connection.release();
            reply(events);
        })
        .catch((err) => {
            reply(boom.badImplementation(new Error(err)));
        });
}

 function submitMeetup(request, reply) {
//
//     connectionPool.getConnection( (err, connection) => {
//         if (err){
//             console.log(err);
//             return reply(boom.badImplementation(new Error('Meetup Request failed - NO DB Connection')));
//         }
//
//
//
//     });
 }

module.exports.register = function (server, options, next) {

    server.route([
        { method: 'GET',  path: '/events', handler: getEvents },
        //{ method: 'POST', path: '/event/meetup', handler: submitMeetup}
    ]);
    next();
};

module.exports.register.attributes = {
    name: 'Events',
    version: '1.0.0'
};
