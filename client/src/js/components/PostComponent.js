import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { Item, Label } from 'semantic-ui-react'

var hljs = require('highlight.js');
import '../../../../node_modules/highlight.js/styles/solarized-light.css'

var Remarkable = require('remarkable');

const md = new Remarkable('full', {
    html:         false,        // Enable HTML tags in source
    xhtmlOut:     true,        // Use '/' to close single tags (<br />)
    breaks:       true,        // Convert '\n' in paragraphs into <br>
    langPrefix:   'language-',  // CSS language prefix for fenced blocks
    linkify:      true,         // autoconvert URL-like texts to links
    linkTarget:   '',           // set target to open link in

    // Enable some language-neutral replacements + quotes beautification
    typographer:  true,

    // Double + single quotes replacement pairs, when typographer enabled,
    // and smartquotes on. Set doubles to '«»' for Russian, '„“' for German.
    quotes: '“”‘’',

    // Highlighter function. Should return escaped HTML,
    // or '' if input not changed
    highlight: function (str, lang) {
        if (lang && hljs.getLanguage(lang)) {
            try {
                return hljs.highlight(lang, str).value;
            } catch (__) {}
        }

        try {
            return hljs.highlightAuto(str).value;
        } catch (__) {}

        return ''; // use external default escaping
    }
});


import { connect } from 'react-redux';
import { Link } from 'react-router-dom';




export default class PostComponent extends Component {

    constructor(props) {
        super(props);
    }

    renderMarkdown(markdown) {
        return md.render(markdown);
    }

    render() {

        let publishedDate = null;
        if (this.props.post &&
            this.props.post.publishedDate) {

            let date = moment(this.props.post.publishedDate);
            publishedDate = date.format('MM-DD-YYYY');
        }

        let urlSafeTitle = this.props.post.urlSafeTitle || '';

        let tags = [];
        if (this.props.post.tags) {
            tags = this.props.post.tags.map((tag) => {
                return <Link to={`/tag/${tag.slug}`} key={`${tag.post_id}-${tag.slug}`}><Label>{tag.name}</Label></Link>
            });
        }

        return (
            <Item>
                <Item.Content>
                    <Item.Header as={Link} to={`/view/${urlSafeTitle}`}>{this.props.post.title}</Item.Header>
                    <Item.Meta>
                        <time className="post-date" dateTime={this.props.post.date}>{publishedDate}</time>
                    </Item.Meta>
                    <Item.Description dangerouslySetInnerHTML={{ __html: this.renderMarkdown(this.props.post.markdown) }} >
                    </Item.Description>
                    <Item.Extra>
                        {tags}
                    </Item.Extra>
                </Item.Content>
            </Item>
        );
    }
}

PostComponent.propTypes = {

};



