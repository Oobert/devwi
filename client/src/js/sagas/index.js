import { put, takeEvery, takeLatest, all, fork, call, apply, select } from 'redux-saga/effects'
import { push } from 'react-router-redux'
import ActionTypes from '../constants/actionTypes';
import { receivePostCount, fetchingPostCount, receivePostsForPage, fetchingPostsForPage, newPostSaving, newPostSaved} from '../actions';
import { API_URL_GENERATOR } from "../constants/routes";


export function* newPostSubmitHandler(){

    yield put(newPostSaving());
    let newPost = yield select((state) => state.newPost);

    let response = yield call(fetch, '/api/post', {method: 'POST', body: JSON.stringify(newPost), headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    } });

    if (response.ok){
        yield put(newPostSaved());
         yield put(push('/'));
    }
    else {
        alert('no worky');
    }
}

export function* requestPostCount(action) {

    let url = null;
    if (action.tag) {
        url = API_URL_GENERATOR.POST_COUNT_FOR_TAG(action.tag);
    } else {
        url = API_URL_GENERATOR.POST_COUNT();
    }

    let postCount = yield select(state => state.postCount);

    if ((!postCount ||
        postCount.postCount === 0) &&
        !postCount.isFetchingPostCount) {

        yield put(fetchingPostCount());
        let response = yield call(fetch, url);
        let postCountFromDb = yield apply(response, response.json);
        yield put(receivePostCount(postCountFromDb));
    }
}

export function* requestPosts(action) {

    let url = null;
    if (action.tag) {
        url = API_URL_GENERATOR.POSTS_FOR_TAG(action.tag, action.pageNumber);
    } else {
        url = API_URL_GENERATOR.POSTS(action.pageNumber);
    }

    yield  put(fetchingPostsForPage());
    let response = yield call(fetch, url);
    let posts = yield apply(response, response.json);
    yield put(receivePostsForPage(posts));

}

export default function* rootSaga() {
    yield takeLatest(ActionTypes.REQUEST_POSTS_FOR_PAGE, requestPosts);
    yield takeLatest(ActionTypes.REQUEST_POSTCOUNT, requestPostCount);
    yield takeEvery(ActionTypes.NEWPOST_SUBMITTED, newPostSubmitHandler);
}