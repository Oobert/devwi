import 'babel-polyfill';//required to make use of ES6 promises in the browser, which fetch (isomorphic-fetch) uses
import 'semantic-ui-css/semantic.min.css';
import '../css/style.css';


import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from './store/configureStore';
import { ConnectedRouter } from 'react-router-redux';
import { history } from './store/configureStore';
import Root from './containers/Root';

const store = configureStore();

render(
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <Root store={store} />
        </ConnectedRouter>
    </Provider>,
    document.getElementById('root'));

