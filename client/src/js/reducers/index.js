import {combineReducers} from 'redux';
import { routerReducer  } from 'react-router-redux';
import postCount from './postCount';
import newPost from './newPost';
import posts from './posts';

const rootReducer = combineReducers({
    router: routerReducer,
    postCount,
    newPost,
    posts
});
export default rootReducer;