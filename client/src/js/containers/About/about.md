## About

DevWi.com's primary mission is to amplify the awesome and exciting that happens in Wisconsin. As it stands, there isn't a great place to find infomartion about Wisconsin anywhere on the internet. It is all scattered through out various outlets. What DevWi.com plans to do is find that content, with your help, and create a link to it. To amplify that content and make it easier to find. 


### But why?

Easy. Because it isn't being done enough. Right now, it doesn't seem like it is being done much at all and that sucks. The ultimate goal is to improve the technical community in Wisconsin and help it grow. Today our community is still very young and it needs help to foster its growth and mature. We are just trying to do our part in that process. 

### How can I help? 

We are glad you asked. 
* First you can get the word out that DevWi.com exists. Tell your friends and co-workers. Heck, even tell your dog. Every little bit helps.
* Next you can tweet @DevWiLLC with great Wisconsin based content that we can amplify. If you or your company has a blog, we would love to know about it.
* Lastly, if you have some spare time, get involved. Attend a meetup or event. Join slack and just network with people. It may not feel like much but it helps us all grow.  