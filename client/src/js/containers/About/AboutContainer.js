import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import PostComponent from '../../components/PostComponent';
let aboutMd = require('./about.md');

class AboutContainer extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        let about = {
            markdown: aboutMd,
            publishedDate: ''
        }


        return (
            <div>
                <PostComponent post={about}/>
            </div>
        )
    }
}

AboutContainer.propTypes = {

};


function mapStateToProps(state) {
    return {};
}


export default withRouter(connect(mapStateToProps, {

})(AboutContainer));