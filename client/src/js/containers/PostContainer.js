import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { Item } from 'semantic-ui-react';

import PostComponent from '../components/PostComponent';



class PostContainer extends Component {

    constructor(props) {
        super(props);

        this.state = {post: null};
    }

    componentDidMount() {
        this.fetchPosts(this.props.match.params.postTitle);
    }

    fetchPosts(pageNumber){
        let requestUrl = '/api/post';
        if (pageNumber){
            requestUrl += '/' + pageNumber;
        }
        fetch(requestUrl).then(res => res.json())
            .then(json => this.setState({post: json}));
    }

    render() {

        if (this.state.post){
           return (<div>
               <Item.Group divided>
                   <PostComponent post={this.state.post}/>
               </Item.Group>
            </div>);

        }
        else {
            return (<div/>);
        }
    }
}

PostContainer.propTypes = {

};


function mapStateToProps(state) {
    return {

    };
}


export default connect(mapStateToProps, {
})(PostContainer)

