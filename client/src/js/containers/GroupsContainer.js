import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Table } from 'semantic-ui-react';
import moment from 'moment';

class GroupsContainer extends Component {

    constructor(props) {
        super(props);

        this.state = {groups: null};
    }

    componentDidMount() {
        this.fetchGroups();
    }

    fetchGroups(){

        fetch('/api/groups').then(res => res.json())
            .then(json => this.setState({groups: json}));
    }

    render() {

        if (this.state.groups){

            let groups = [];
            this.state.groups.forEach((group) => {

                let start = '';
                let title = null;
                if (group.event) {
                    let startDate = moment(group.event.start);
                    start = startDate.format('MM-DD-YYYY');
                    title = (
                        <a href={group.event.eventUrl}>{group.event.title}</a>
                    );
                }

               groups.push(
                   <Table.Row key={group.meetupGroupId}>
                       <Table.Cell><a href={group.url}>{group.name}</a></Table.Cell>
                       <Table.Cell>{title}</Table.Cell>
                       <Table.Cell>{start}</Table.Cell>
                   </Table.Row>);
            });

            return (
                <Table celled striped>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>Meetup Name</Table.HeaderCell>
                            <Table.HeaderCell>Upcoming Event</Table.HeaderCell>
                            <Table.HeaderCell>Upcoming Event Date</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>
                    <Table.Body>
                        {groups}
                    </Table.Body>
                </Table>
            );

        }
        else {
            return (<div/>);
        }
    }
}

GroupsContainer.propTypes = {

};


function mapStateToProps(state) {
    return {

    };
}


export default connect(mapStateToProps, {
})(GroupsContainer)

