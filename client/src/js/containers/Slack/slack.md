## Wisconsin Slack Communities

Slack is a persistent group chat application and has become the default for community chat. It is simple to use and setup. Head over to [Slack's website](https://slack.com/) to download the click on your platform of choice. Then all that is left to join one of Wisconsin's active communities. 



| Name        | Join Link           | 
| ------------- |:-------------| 
| Milwaukee Slack | http://mke-slack.herokuapp.com/ | 
| Madison Slack | http://madisoncommunity.org/ |
| Northeast WI Slack | http://newslack.appletonmakerspace.org/ |
|  That Conference Slack* | http://thatslack.thatconference.com/ |


* The That Conference slack isn't technically a Wisconsin slack. However the conference is held here and many people in the slack are from Wisconsin. It seemed fitting to include it. 