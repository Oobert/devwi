import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import PostComponent from '../../components/PostComponent';
let slackMd = require('./slack.md');

class SlackContainer extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        let slack = {
            markdown: slackMd,
            publishedDate: ''
        }


        return (
            <div>
                <PostComponent post={slack}/>
            </div>
        )
    }
}

SlackContainer.propTypes = {

};


function mapStateToProps(state) {
    return {};
}


export default withRouter(connect(mapStateToProps, {

})(SlackContainer));