import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { findDOMNode } from 'react-dom';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom'
import '../../../..//node_modules/fullcalendar/dist/fullcalendar.css';

import $ from 'jquery';
import moment from 'moment';
import calender from 'fullcalendar';


class CalendarContainer extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        $(findDOMNode(this)).fullCalendar({
            timezone: 'local',
            eventSources: [
                {
                    url: '/api/events',
                }
            ]
        });
    }

    render() {



        return (
            <div id="calendar"></div>
        )
    }
}

CalendarContainer.propTypes = {

};


function mapStateToProps(state) {
    return {};
}


export default withRouter(connect(mapStateToProps, {

})(CalendarContainer))