import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux';
import { Item } from 'semantic-ui-react';

import { requestPostCount, requestPostsForPage } from '../actions/';

import PostComponent from '../components/PostComponent';
import PaginationComponent from '../components/PaginationComponent';


class PostListContainer extends Component {

    constructor(props) {
        super(props);

        this.state = {posts: null};
    }

    componentDidMount() {
        this.props.requestPostCount(this.props.match.params.tag);
        this.props.requestPostsForPage(this.props.match.params.pageNumber,  this.props.match.params.tag);
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.match.params.pageNumber !== nextProps.match.params.pageNumber &&
            !this.props.posts.isFetchingPosts){
            this.props.requestPostsForPage(nextProps.match.params.pageNumber, nextProps.props.match.params.tag);
        }
    }

    render() {
        
        if (this.props.posts.posts){
            let posts = [];
            this.props.posts.posts.forEach(post => {
                posts.push(<PostComponent key={post.id} post={post}/>)
            });

            return (<div>
                <Item.Group divided>
                {posts}
                </Item.Group>
                <PaginationComponent postCount={this.props.postCount.postCount} pageNumber={Number(this.props.match.params.pageNumber) || 0} />
            </div>);

        }
        else {
            return (<div/>);
        }
    }
}

PostListContainer.propTypes = {
    requestPostCount: PropTypes.func.isRequired
};


function mapStateToProps(state) {
    return {
        postCount: state.postCount,
        posts: state.posts
    };
}


export default withRouter(connect(mapStateToProps, {
    requestPostCount,
    requestPostsForPage
})(PostListContainer))

