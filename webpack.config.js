var webpack = require('webpack');
var path = require('path');
var OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
// used to inject the bundle directly into index.html
var HtmlWebpackPlugin = require('html-webpack-plugin');

// used to copy files to the dist folder. In this case the data folder
var CopyWebpackPlugin = require('copy-webpack-plugin');

// cleans out the dist folder on every build
var CleanWebpackPlugin = require('clean-webpack-plugin');

// Force writes dist folder when using webpack-dev-server
// This is need to that the webpack dev server will serve the data
// folder other wise it wont because it is not part of the js bundle.
var WriteFilePlugin = require('write-file-webpack-plugin');

module.exports = {
    entry: {
        main: './client/src/js/app.js'
    },
    output: {
        filename: '[name].[chunkhash].js',
        path: path.resolve(__dirname, 'dist')
    },

    devtool: 'source-map',

    module: {
        rules: [
            {test: /\.html$/, loader: 'html-loader'},
            {test: /\.css$/, use: ExtractTextPlugin.extract({
                fallback: "style-loader",
                use: "css-loader"
            })},
            {test: /\.(png|jpg|jpeg|svg|eot|woff2|woff|ttf)$/, loader: 'file-loader'},
            {test: /\.(md)$/, loader: 'raw-loader'},
            {
                test: /\.js$/, loader: 'babel-loader', exclude: [/node_modules/, /\.spec\.js/],
                options: {
                    presets: ["react", "stage-0", ["env", {
                        "targets": {
                            "browsers": ["last 2 versions", "safari >= 7"]
                        }
                    }]
                    ]
                }
            }
        ],
    },
    plugins: process.env.NODE_ENV === 'production' ? [ //prod
            new webpack.DefinePlugin({
                'process.env': {
                    NODE_ENV: JSON.stringify('production')
                }
            }),
            new CleanWebpackPlugin(['dist'], {}),
            new CopyWebpackPlugin([
                { from: 'client/src/fav' },
            ]),
            new ExtractTextPlugin("styles.[contenthash].css"),
            new OptimizeCssAssetsPlugin({
                assetNameRegExp: /\.css$/g,
                cssProcessor: require('cssnano'),
                cssProcessorOptions: { discardComments: {removeAll: true } },
                canPrint: true
            }),
            new webpack.optimize.CommonsChunkPlugin({
                name: "vendor",
                minChunks: function(module){
                    return module.context && module.context.indexOf("node_modules") !== -1;
                }
            }),
            new HtmlWebpackPlugin({
                template: 'client/index.html',
                inject: 'body'
            }),
            new webpack.optimize.OccurrenceOrderPlugin(),
            new webpack.optimize.UglifyJsPlugin(),
        ] : [ //dev
            new CleanWebpackPlugin(['dist'], {}),
            new CopyWebpackPlugin([
                { from: 'client/src/fav' },
            ]),
            new ExtractTextPlugin("styles.[contenthash].css"),
            new webpack.optimize.CommonsChunkPlugin({
                name: "vendor",
                minChunks: function(module){
                    return module.context && module.context.indexOf("node_modules") !== -1;
                }
            }),
            new WriteFilePlugin(),
            new HtmlWebpackPlugin({
                template: 'client/index.html',
                inject: 'body'
            })
        ]
};
